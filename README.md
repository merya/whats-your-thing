# Core idea 

Ease the tool-seeking process for the phd project and/or daily life 

# How ?

1. 5-10' presentation at the end of phd seminars 
	- it's rather an *invitation* to discover the tool than a tutorial of it

2. gitlab project on the UGA instance (gricad) to gather all materials
	- be as *encyclopedic* as you need  


# Structure of a WYT presentation

- Introduce a particular problem that you have encounter 
- explain how your tool can solve / bypass it | how does it work ?
- give an insight of how to use it and estimate the || brief demonstration
- How long is to become handy with it ?  
- Share your website, app, software, book, article, anything that you find useful

# Workforce 

- Up to 2 persons can present per phd seminars
- 3 persons to keep up to date the gitlab project
- 1 person for communication and finding next speakers 

